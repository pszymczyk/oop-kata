package com.pszymczyk.roleobject;

abstract class PersonRole {

    abstract boolean hasType(String roleName);

}
