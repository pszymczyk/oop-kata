package com.pszymczyk.party;

import java.util.Set;

/**
 * @author pawel szymczyk
 */
class Mother extends AccountabilityType {

    protected Mother(Set<Accountability> accountabilities) {
        super(accountabilities);
    }
}
