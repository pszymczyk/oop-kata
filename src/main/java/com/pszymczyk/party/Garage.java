package com.pszymczyk.party;

import java.util.Set;

/**
 * @author pawel szymczyk
 */
class Garage extends AccountabilityType {
    protected Garage(Set<Accountability> accountabilities) {
        super(accountabilities);
    }
}
