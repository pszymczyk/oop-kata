package com.pszymczyk.party;

import java.util.Set;

/**
 * @author pawel szymczyk
 */
class Hospital extends AccountabilityType {
    protected Hospital(Set<Accountability> accountabilities) {
        super(accountabilities);
    }
}
