package com.pszymczyk.party;

import java.util.Set;

/**
 * @author pawel szymczyk
 */
class Doctor extends AccountabilityType {

    protected Doctor(Set<Accountability> accountabilities) {
        super(accountabilities);
    }
}
