package com.pszymczyk.gearbox;

interface EngineMonitoringSystem {
    int getCurrentRPM();
}
