package com.pszymczyk.gearbox;

enum Mode {
    ECO, COMFORT, SPORT, SPORT_PLUS;
}
