package com.pszymczyk.gearbox;

interface GearBox {

    void changeGear(int gear);

    int getCurrentGear();
}
