| TYPE    |VALUE                                             |
|---------|--------------------------------------------------|
| KM      | { "feePerMinute": "5PLN" }                       |
| SLAM    | { "feePerMinute": "7PLN" }                       |
| TRAFFIC | { "waitingClients": 100, "availableCars": 20 }   |