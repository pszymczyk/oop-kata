package com.pszymczyk.inheritance.problem;

import com.pszymczyk.inheritance.problem.external.Wear;

/**
 * @author pawel szymczyk
 */
class WearAvailabilityNotifier {
    void notify(Wear wear) {

    }
}
